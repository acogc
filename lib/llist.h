/*
    llist.h - simple intrusive cyclic double linked list

  Copyright (C) 2003, 2005, Christian Thaeter <chth@gmx.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 2 as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, contact me.

*/

#ifndef LLIST_H
#define LLIST_H

#include <stddef.h>
#include <assert.h>

#ifndef LLIST_SPECIAL_BUILD
#define LLIST_INTERFACE
#define LLIST_IMPLEMENTATION
#endif

#ifdef HAVE_INLINE
#       define LLIST_MACRO static inline
#else
#       ifdef __GNUC__
#               define LLIST_MACRO static __inline__
#       else
#               define LLIST_MACRO static
#       endif
#endif

#ifdef LLIST_INTERFACE

/* we have a llist node in a structure and want to get the structure which embedds it */
/*
//struct foo
//{
// int bar;
// llist l;
//} x;

LLIST_TO_STRUCTP (&x.l, foo, l)->bar
*/
#define LLIST_TO_STRUCTP(llist, type, member) \
  ((type*)(((char*)(llist)) - offsetof(type, member)))

#define LLIST_FOREACH(list, node)               \
  if (!list); else                              \
    for (LList node = llist_get_head (list);    \
         ! llist_is_end (node, list);           \
         llist_forward (&node))

#define LLIST_FOREACH_REV(list, node)           \
  if (!list); else                              \
    for (LList node = llist_get_tail (list);    \
         ! llist_is_end (node, list);           \
         llist_backward (&node))

#define LLIST_WHILE_HEAD(list, head)            \
  if (!list); else                              \
    for (LList head = llist_get_head (list);    \
         !llist_is_empty (list);                \
         head = llist_get_head (list))

#define LLIST_WHILE_TAIL(list, tail)            \
  if (!list); else                              \
    for (LList tail = llist_get_tail (list);    \
         !llist_is_empty (list);                \
         tail = llist_get_tail (list))


/*
 * Type of a llist node.
 */
struct llist_struct
{
  struct llist_struct *next;
  struct llist_struct *prev;
};
typedef struct llist_struct llist;
typedef llist * LList;
typedef const llist * const_LList;
typedef llist ** LList_ref;

/*
 * handy macro to instantiate a local llist 'name' becomes a LList handle and
 * the underlying instance is hidden as name_llist_, this list is statically initialized
 */
#define LLIST_INITIALIZER(name) {&name,&name}
#define LLIST_AUTO(name) \
llist name##_llist_ = LLIST_STATIC_INITIALIZER(name##_llist_);\
LList name = &name##_llist_

LLIST_MACRO void llist_init (LList self);
LLIST_MACRO int llist_is_empty (const_LList self);
LLIST_MACRO int llist_is_single (const_LList self);
LLIST_MACRO int llist_is_head (const_LList self, const_LList list);
LLIST_MACRO int llist_is_tail (const_LList self, const_LList list);
LLIST_MACRO int llist_is_end (const_LList self, const_LList list);
LLIST_MACRO int llist_is_member (const_LList self, const_LList list);
LLIST_MACRO int llist_is_before (const_LList self, const_LList other, const_LList list);
LLIST_MACRO int llist_is_after (const_LList self, const_LList other, const_LList list);
LLIST_MACRO unsigned llist_count (const_LList self);
LLIST_MACRO unsigned llist_distance (const_LList self, const_LList other, const_LList list);
LLIST_MACRO void llist_unlink_fast_ (LList self);
LLIST_MACRO LList llist_unlink (LList self);
LLIST_MACRO LList llist_relocate (LList self);
LLIST_MACRO LList llist_insert_before (LList self, LList successor);
LLIST_MACRO LList llist_insert_after (LList self, LList predcessor);
LLIST_MACRO LList llist_insert_list_before (LList self, LList successor);
LLIST_MACRO LList llist_insert_list_after (LList self, LList predcessor);
LLIST_MACRO LList llist_insert_range_before (LList self, LList start, LList end);
LLIST_MACRO LList llist_insert_range_after (LList self, LList start, LList end);
LLIST_MACRO LList llist_advance (LList self);
LLIST_MACRO LList llist_retreat (LList self);
LLIST_MACRO LList llist_get_next (const_LList self);
LLIST_MACRO LList llist_get_prev (const_LList self);
LLIST_MACRO void llist_forward (LList_ref self);
LLIST_MACRO void llist_backward (LList_ref self);
LLIST_MACRO LList llist_get_nth (LList self, int n);

#endif /*LLIST_INTERFACE*/



#ifdef LLIST_IMPLEMENTATION

#define llist_insert_head(list,element) llist_insert_after(element,list)
#define llist_insert_tail(list,element) llist_insert_before(element,list)
#define llist_get_head llist_get_next
#define llist_get_tail llist_get_prev


/*
 * initialize a new llist. Must not be
 * applied to a list which is not empty! Lists need to be initialized
 * before any other operation on them is called.
 */
LLIST_MACRO void
llist_init (LList self)
{
  self->next = self->prev = self;
}

/*
 * check if a node is linked with some other node
 */
LLIST_MACRO int
llist_is_empty (const_LList self)
{
  return self->next == self;
}

/*
 * check if self is the only node in a list or self is not in a list
 */
LLIST_MACRO int
llist_is_single (const_LList self)
{
  return self->next->next == self;
}

/*
 * check if self is the head of list
 */
LLIST_MACRO int
llist_is_head (const_LList self, const_LList list)
{
  return list->next == self;
}

/*
 * check if self is the tail of list
 */
LLIST_MACRO int
llist_is_tail (const_LList self, const_LList list)
{
  return list->prev == self;
}

/*
 * check if self is the end of list
 */
LLIST_MACRO int
llist_is_end (const_LList self, const_LList list)
{
  return list == self;
}

/*
 * check if self is a member of list
 */
LLIST_MACRO int
llist_is_member (const_LList self, const_LList list)
{
  const_LList i = self->next;
  for (; i != self; i = i->next)
    {
      if (i == list)
        return 1;
    }
  return 0;
}

/*
 * check if self is before other in list
 */
LLIST_MACRO int
llist_is_before (const_LList self, const_LList other, const_LList list)
{
  assert (llist_is_member (self, list));
  assert (llist_is_member (other, list));
  const_LList i = self->next;
  if (self != other)
    {
      for (; i != list; i = i->next)
        {
          if (i == other)
            return 1;
        }
    }
  return 0;
}

/*
 * check if self is after other in list
 */
LLIST_MACRO int
llist_is_after (const_LList self, const_LList other, const_LList list)
{
  assert (llist_is_member (self, list));
  assert (llist_is_member (other, list));
  const_LList i = self->prev;
  if (self != other)
    {
      for (; i != list; i = i->prev)
        {
          if (i == other)
            return 1;
        }
    }
  return 0;
}


/*
 * count the nodes of self
 */
LLIST_MACRO unsigned
llist_count (const_LList self)
{
  unsigned cnt = 0;
  const_LList i = self;
  for (; i->next != self; ++cnt, i = i->next);
  return cnt;
}

/*
 * count distance from self to other in list
 */
LLIST_MACRO unsigned
llist_distance (const_LList self, const_LList other, const_LList list)
{
  assert (llist_is_member (self, list));
  assert (llist_is_member (other, list));

  unsigned cnt = 0;
  const_LList i = self;
  for (; i->next != other && i->next != list; ++cnt, i = i->next);
  if (i->next == list)
    for (cnt = 0, i = self; i->prev != other; ++cnt, i = i->prev);
  return cnt;
}

/* private */
LLIST_MACRO void
llist_unlink_fast_ (LList self)
{
  LList nxt = self->next, pre = self->prev;
  nxt->prev = pre;
  pre->next = nxt;
}

/*
 * removes self from its list
 */
LLIST_MACRO LList
llist_unlink (LList self)
{
  llist_unlink_fast_ (self);
  return self->next = self->prev = self;
}

/*
 * fixes a list if self got relocated in memory
 */
LLIST_MACRO LList
llist_relocate (LList self)
{
  return self->next->prev = self->prev->next = self;
}

/*
 * inserts self before successor,
 * self can already linked to a list where it will be removed
 */
LLIST_MACRO LList
llist_insert_before (LList self, LList successor)
{
  llist_unlink_fast_ (self);
  self->prev = successor->prev;
  self->next = successor;
  return successor->prev = self->prev->next = self;
}


/*
 * inserts self after predcessor,
 * self can already linked to a list where it will be removed
 */
LLIST_MACRO LList
llist_insert_after (LList self, LList predcessor)
{
  llist_unlink_fast_ (self);
  self->next = predcessor->next;
  self->prev = predcessor;
  return predcessor->next = self->next->prev = self;
}

/*
 * move the content of list self before node successor
 */
LLIST_MACRO LList
llist_insert_list_before (LList self, LList successor)
{
  if (!llist_is_empty (self))
    {
      self->next->prev = successor->prev;
      self->prev->next = successor;
      successor->prev->next = self->next;
      successor->prev = self->prev;
      self->prev = self->next = self;
    }
  return self;
}

/*
 * move the content of list self after node predcessor
 */
LLIST_MACRO LList
llist_insert_list_after (LList self, LList predcessor)
{
  if (!llist_is_empty (self))
    {
      self->prev->next = predcessor->next;
      self->next->prev = predcessor;
      predcessor->next->prev = self->prev;
      predcessor->next = self->next;
      self->prev = self->next = self;
    }
  return self;
}

/*
 * move members from start to end->prev before self
 */
LLIST_MACRO
LList llist_insert_range_before (LList self, LList start, LList end)
{
  LList tmp = self->prev;
  start->prev->next = end;
  end->prev->next = self;
  self->prev->next = start;

  self->prev = end->prev;
  end->prev = start->prev;
  start->prev = tmp;

  return self;
}

/*
 * move members from start to end->prev after self
 */
LLIST_MACRO
LList llist_insert_range_after (LList self, LList start, LList end)
{
  start->prev->next = end;
  end->prev->next = self->next;
  self->next->prev = end->prev;
  self->next = start;
  end->prev = start->prev;
  start->prev = self;
  return self;
}

/*
 * swap a node with its next node
 */
LLIST_MACRO LList
llist_advance (LList self)
{
  LList tmp = self->next->next;
  tmp->prev = self;
  self->next->prev = self->prev;
  self->prev->next = self->next;
  self->prev = self->next;
  self->next->next = self;
  self->next = tmp;
  return self;
}

/*
 * swap a node with its previous node
 */
LLIST_MACRO LList
llist_retreat (LList self)
{
  LList tmp = self->prev->prev;
  tmp->next = self;
  self->prev->next = self->next;
  self->next->prev = self->prev;
  self->next = self->prev;
  self->prev->prev = self;
  self->prev = tmp;
  return self;
}



/*
 * return the member after self
 */
LLIST_MACRO LList
llist_get_next (const_LList self)
{
  return self->next;
}

/*
 * return the member before self
 */
LLIST_MACRO LList
llist_get_prev (const_LList self)
{
  return self->prev;
}

/*
 * put self to the next node
 */
LLIST_MACRO void
llist_forward (LList_ref self)
{
  *self = (*self)->next;
}

/*
 * put self to the previous node
 */
LLIST_MACRO void
llist_backward (LList_ref self)
{
  *self = (*self)->prev;
}

/*
 * get the nth element after (positive n) or before (negative n) self
 */
LLIST_MACRO LList
llist_get_nth (LList self, int n)
{
  if (n>0)
    while (n--)
      self = llist_get_next (self);
  else
    while (n++)
      self = llist_get_prev (self);

  return (LList) self;
}

#endif /*LLIST_IMPLEMENTATION*/

#endif /*LLIST_H */
/*
// Local Variables:
// mode: C
// c-file-style: "gnu"
// End:
// arch-tag: e8fe4a59-fd55-4c45-b860-5cd1e0771213
// end_of_file
*/
