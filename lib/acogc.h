/*
  acogc.h - simple accurate/cooperative garbage collector

  Copyright (C) 2006, Christian Thaeter <chth@gmx.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 2 as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, contact me.

*/

#ifndef ACOGC_H
#define ACOGC_H

/*
accurate
  only real references are used for marking objects instead scanning all objects

cooperative
  the programmer has to supply functions which mark references in his datastructures
*/

#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <limits.h>
#include <nobug.h>

#include "llist.h"

NOBUG_DECLARE_FLAG(acogc);
NOBUG_DECLARE_FLAG(acogc_mark);
NOBUG_DECLARE_FLAG(acogc_collect);
NOBUG_DECLARE_FLAG(acogc_alloc);
NOBUG_DECLARE_FLAG(acogc_weak);


/*
  object types and handles
*/
#define ACOGC_DECLARE(name,Handle) \
  struct acogc_##name##_struct; \
  typedef struct acogc_##name##_struct acogc_##name;\
  typedef const acogc_##name * const_Acogc##Handle;\
  typedef acogc_##name * Acogc##Handle;\
  typedef acogc_##name ** Acogc##Handle##_ref;\
  typedef const acogc_##name ** const_Acogc##Handle##_ref

ACOGC_DECLARE(root,Root);
ACOGC_DECLARE(stack,Stack);
ACOGC_DECLARE(factory,Factory);
ACOGC_DECLARE(object,Object);
ACOGC_DECLARE(weakref,Weakref);

/*
  Macros
 */

/*
  uncollectable objects (static or stack) have to be defined with this macro
*/
#define ACOGC_OBJECT(state, factory, type, name, ...)           \
      struct { acogc_object acogc_header; type object;}         \
      name ## _acogc = {ACOGC_OBJECT_INITIALIZER( state,        \
                        factory, name),                         \
                        __VA_ARGS__};                           \
      type* name=&name ## _acogc.object

#define ACOGC_OBJECT_INITIALIZER(state, factory, name)          \
      {LLIST_INITIALIZER (name ## _acogc.acogc_header.node ),   \
       factory, NULL, ACOGC_STATE_ ## state}

/*
  tracking pointers on the stack with the GC
*/
struct acogc_stack_struct
{
  void* ptr;
  AcogcStack prev;
};

#define ACOGC_STACK_ENTER(gcroot)                                                               \
AcogcStack_ref acogc_stack_root = &(gcroot)->stack;                                             \
AcogcStack acogc_stack_entry NOBUG_CLEANUP (acogc_assert_stackframeleft) = *acogc_stack_root

#define ACOGC_STACK_PTR(ptrtype, name)          \
union {                                         \
  acogc_stack stack;                            \
  ptrtype ptr;                                  \
}name; name.ptr = NULL;                         \
name.stack.prev = *acogc_stack_root;            \
*acogc_stack_root = &name.stack

// TODO #define ACOGC_STACK_FORGET

#define ACOGC_STACK_LEAVE                               \
*acogc_stack_root = acogc_stack_entry;                  \
acogc_stack_entry = (AcogcStack)&acogc_stack_entry

void
acogc_assert_stackframeleft (AcogcStack_ref p);

/* declare a weak reference als automatic variable, asserts that the reference gets unlinked when the stackframe is left */
#define ACOGC_WEAK_REFERENCE(name) acogc_weakref name NOBUG_CLEANUP(acogc_weakref_assert_cleared) = ACOGC_WEAKREF_INITIALIZER
#define ACOGC_WEAKREF_INITIALIZER {NULL, NULL}


/*
  freeing policy:
  in case no memory is available the GC will gradually free more unused memory
  when this has no effect it calls a user setable routine 3 times to give the user a chance to handle the situation
  this user routine gets the current policy and a pointer to supplemental data (hint: use a jmp_buf) as parameter
  if all fails it will call abort() finally
*/
typedef enum
  {
    ACOGC_COLLECT_DONTFREE,     /* just collect, do not free */
    ACOGC_COLLECT_NORMAL,       /* normal collection freeing down to highwater */
    ACOGC_COLLECT_FORCEMID,     /* freeing down to (lowwater+highwater)/2 */
    ACOGC_COLLECT_FORCELOW,     /* freeing down to lowwater */
    ACOGC_COLLECT_FORCEALL,     /* free all freeable objects */
    ACOGC_COLLECT_USER1,        /* call userhook first time (free some mem) */
    ACOGC_COLLECT_USER2,        /* call userhook second time (free memory hardly) */
    ACOGC_COLLECT_USER3,        /* call userhook third time (handle memory failure or free memory even harder) */
    ACOGC_COLLECT_FAILED        /* still no memory available, will abort() */
  } acogc_freeing_policy;

typedef enum
  {
    ACOGC_STATE_ROOT = -1,
    ACOGC_STATE_UNCOLLECTABLE_NONFINALIZEABLE,
    ACOGC_STATE_UNCOLLECTABLE,
    ACOGC_STATE_BUSY,
    ACOGC_STATE_FIRST,
    ACOGC_STATE_START,
    ACOGC_STATE_LAST = INT_MAX
  } acogc_object_state;

typedef enum
  {
    ACOGC_COLLECT,              /* collect the object */
    ACOGC_KEEP,                 /* let the GC mark the object */
    ACOGC_KEPT,                 /* returned to the user if the object was already marked */
    ACOGC_LAST,                 /* manual tail-call optimization in effect */
  } acogc_mark_result;

typedef enum
  {
    // first states are not GC controlled and memory tagged with them has no gc descriptor
    ACOGC_UNSET,        /* undefined */
    ACOGC_ALLOC,        /* malloc/free acogoc_alloc/acogc_free */
    // next states for memory with gc descriptor
    ACOGC_STATIC,       /* non freeable */
    ACOGC_GC,           /* garbage collected */
  } acogc_alloc_type;

/*
  function types
*/
typedef void (*acogc_initize_func)(void*);
typedef void (*acogc_finalize_func)(void*);
typedef acogc_mark_result (*acogc_mark_func)(void*);

/* nobug init */
void acogc_nobug_init();

/*
  root

  the acogc_root manages all memory allocated with acogc.
  A program can use more than one acogc_root for implementing diffrent memory domains which must not cross-reference objects
*/
struct acogc_root_struct
{
  llist factories;

  int state;                            /* actually a counter, 0 is reserved for root objects */

  AcogcStack stack;

  unsigned long allocation_counter;     /* counts every allocation since the last collection */
  unsigned long collection_freq;        /* number of objects allocated since last collection */

  AcogcObject last;                     /* buffer for last-call marking*/

  void (*nomemhandler)(void*, acogc_freeing_policy);    /* called when no memory can be allocated */
  void* nomemsupp;                                      /* supplemental data for the nomem handler*/
};


/*
  initialize a acogc_root structure
*/
void
acogc_root_init (AcogcRoot self);


/*
  free all memory associated with a acogc_root,
*/
void
acogc_root_erase (AcogcRoot self);

/*
  do a collection, ususaly automatically triggered
*/
void
acogc_root_collect (AcogcRoot self, acogc_freeing_policy pol);


/*
  factory

  the factory holds the static size for all objects it manages.
  Objects are allocated with 'acogc_factory_alloc'.
  Collected objects are cached and freed based on the high_water/low_water policies.
  Collected, not-yet-freed objects can be reinstantiated and
  the user can implement fancy caching schemes.
*/
struct acogc_factory_struct
{
  AcogcRoot root;                       /* points back to the root */
  llist factories;                      /* links all factories of one root together */

  llist roots;                          /* root objects for this factory */
  llist alive;                          /* objects in use */
  llist dead;                           /* unused objects (might still be referenced by weak pointers and reinstantiated on demand) */
  llist tmp;                            /* temporary list for collection */

  unsigned long objects_allocated;      /* grand number of objects (including free) */
  unsigned long objects_used;           /* number of objects in use at the last collection */
  unsigned long objects_new;            /* number of objects allocated since last collection */

  unsigned low_water;                   /* allocate more when less than this % of objects are unused */
  unsigned high_water;                  /* start freeing when more than this % of objects are unused */

  size_t size;                          /* size od objects allocated by this factory */

  acogc_mark_func mark;                 /* marker function */
  acogc_initize_func initize;           /* initizer for NULL'ing referencess */
  acogc_finalize_func finalize;         /* finalizer called before a object is free()'ed */

  const char* name;                     /* for typecheck */
};

/*
  initialize a acogc_factory
*/
void
acogc_factory_init (AcogcFactory self,
                    AcogcRoot root,
                    size_t size,
                    unsigned low_water,
                    unsigned high_water,
                    acogc_mark_func mark,
                    acogc_initize_func initize,
                    acogc_finalize_func finalize,
                    const char* name);


/* alloc memory from factory */
void *
acogc_factory_alloc (AcogcFactory self);


/*
  object

  a acogc_object prepends each user allocated block
*/
struct acogc_object_struct
{
  llist node;
  AcogcFactory factory;
  AcogcWeakref weakrefs;
  int state;
};

/* register a object as GC root object, should be done as soon as possible after allocation */
void
acogc_addroot (void* object);

/* unregister a GC root object, it becomes normal collectable by this. */
void
acogc_removeroot (void* object);

/*
  object
 */
static inline void*
acogc_memory_from_object (const_AcogcObject const self)
{
  return (void*)self + sizeof (acogc_object);
}

static inline AcogcObject
acogc_object_from_memory (const void * const self)
{
  return (AcogcObject)(self - sizeof (acogc_object));
}

/*
  factories can be used to identify the type of a object
 */
static inline AcogcFactory
acogc_object_type (const void * o)
{
  REQUIRE (o);
  return acogc_object_from_memory (o)->factory;
}

/*
  typecheck which uses the name as literal string
 */
static inline int
acogc_object_typecheck (const void * o, const char* name)
{
  REQUIRE (o);
  REQUIRE (name);
  if (acogc_object_from_memory (o)->factory->name == name)
    return 1;
  else
    return !strcmp (acogc_object_from_memory (o)->factory->name, name);
}

/*
  marking objects
*/
acogc_mark_result
acogc_object_markreally (AcogcObject object);

static inline acogc_mark_result
acogc_object_mark (void * o)
{
  TRACE(acogc_mark, "%p", o);

  if (o)
    {
      AcogcObject object = acogc_object_from_memory (o);

      TRACE_DBG (acogc_mark, "state %d", object->state);
      TRACE_DBG (acogc_mark, "factory %p", object->factory);
      TRACE_DBG (acogc_mark, "factory->root %p", object->factory->root);
      TRACE_DBG (acogc_mark, "factory->root->state %d", object->factory->root->state);

      if (object->state >= ACOGC_STATE_FIRST && object->state < object->factory->root->state)
        {
          /* not yet marked, mark now */
          object->state = ACOGC_STATE_BUSY;
          return acogc_object_markreally (object);
        }
      else
        return ACOGC_KEPT;      /* already marked */
    }
  return ACOGC_COLLECT;         /* when o was NULL */
}

static inline acogc_mark_result
acogc_object_lastmark (void * o)
{
  TRACE(acogc_mark, "lastmark %p", o);

  if (o)
    {
      INFO (acogc_mark, "object %p", o);
      AcogcObject object = acogc_object_from_memory (o);

      TRACE_DBG (acogc_mark, "state %d", object->state);
      TRACE_DBG (acogc_mark, "factory %p", object->factory);
      TRACE_DBG (acogc_mark, "factory->root %p", object->factory->root);
      TRACE_DBG (acogc_mark, "factory->root->state %d", object->factory->root->state);

      if (object->state >= ACOGC_STATE_FIRST && object->state < object->factory->root->state)
        {
          ASSERT (object->factory->root->last == NULL, "last_mark not last in a marker");

          object->state = ACOGC_STATE_BUSY;
          object->factory->root->last = object;
          return ACOGC_KEEP;
          //return acogc_object_markreally (object);
        }
      else
        return ACOGC_KEPT;
    }
  return ACOGC_COLLECT;       // when o was NULL
}


/*
  weakrefs

  weak references are lists which must be maintained by the programmer
*/
struct acogc_weakref_struct
{
  AcogcWeakref next;
  void * ref;
};

static inline void
acogc_weakref_init (AcogcWeakref self)
{
  REQUIRE (self);
  self->next = self->ref = NULL;
}

void
acogc_weakref_link (AcogcWeakref self, void* object);

void
acogc_weakref_unlink (AcogcWeakref self);

void
acogc_object_weakref_invalidate (void* memory);


/*
  when a object is swept but still not freed it can be reinstatiated (for example if a weak-reference points to it)
*/

/* dereference a weak reference, reinstantiate it if required */
void *
acogc_weakref_reinstget (AcogcWeakref self);

/* dereference a weak reference, do not reinstantiate it if swept, return NULL */
void *
acogc_weakref_get (AcogcWeakref self);

void
acogc_weakref_assert_cleared (AcogcWeakref p);

/*
  manual allocation not garbagge collected, needs to be acogc_free()'d
  the advantage of these are that malloc will initiate a garbagge collection
  if no memory is available and call the registered nomemhandler.
  This functions are guranteed to be interchangeable with malloc/realloc/free as
  long the pointer doesn't hold a reference, see below.
*/

/*
  try to (re-)allocate some block of memory with a new size
  *addr has to be initialized to NULL
  trying to reallocate a reference is illegal.

  unfotunally we need some ugly casts here
*/
void
acogc_alloc_ (void ** addr, size_t size, AcogcRoot factory);
#define acogc_alloc(addr, size, root) acogc_alloc_ ((void*)addr, size, root)

/*
  realloc a memory block to at least needed size, min 16 bytes, increased by a 1.5 factor on resizing
  return the amount really reserved
  when resizing is finsished one might fixate it by calling acogc_alloc with the real required size at last,
  some realloc implementations might free some memory thereafter.
*/
size_t
acogc_reserve_ (void ** addr, size_t actual, size_t needed, AcogcRoot root);
#define acogc_reserve(addr, actual, needed, root) acogc_reserve_ ((void*)addr, actual, needed, root)

/*
  free *addr if not a reference and set it to NULL
*/
void
acogc_free_ (void ** addr);
#define acogc_free(addr) acogc_free_ ((void*)addr)

void
acogc_assert_stackframeleft (AcogcStack_ref p);

#endif
/*
// Local Variables:
// mode: C
// c-file-style: "gnu"
// End:
// arch-tag: 855b7025-8227-4892-ac25-9b0a3d57dd0b
// end_of_file
*/
