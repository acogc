/*
  acogc.c - simple accurate/cooperative garbage collector

  Copyright (C) 2006, Christian Thaeter <chth@gmx.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 2 as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, contact me.

*/

#include <assert.h>
#include <stdlib.h>

#include "acogc.h"
// TODO collection statistics (timing, global counter, etc)

/* nobug init*/
NOBUG_DEFINE_FLAG(acogc);
NOBUG_DEFINE_FLAG_PARENT(acogc_mark, acogc);
NOBUG_DEFINE_FLAG_PARENT(acogc_collect, acogc);
NOBUG_DEFINE_FLAG_PARENT(acogc_alloc, acogc);
NOBUG_DEFINE_FLAG_PARENT(acogc_weak, acogc);

void acogc_nobug_init()
{
  NOBUG_INIT_FLAG(acogc);
  NOBUG_INIT_FLAG(acogc_mark);
  NOBUG_INIT_FLAG(acogc_collect);
  NOBUG_INIT_FLAG(acogc_alloc);
  NOBUG_INIT_FLAG(acogc_weak);
}

/*
  root
*/
void
acogc_root_init (AcogcRoot self)
{
  REQUIRE (self);
  NOTICE (acogc, "self %p", self);

  llist_init (&self->factories);
  self->allocation_counter = 0;
  self->collection_freq = 32;
  self->stack = NULL;
  self->last = NULL;
  self->nomemhandler = NULL;
  self->nomemsupp = NULL;
  self->state = ACOGC_STATE_FIRST;
}


void
acogc_root_erase (AcogcRoot self)
{
  REQUIRE (self);
  NOTICE (acogc, "self %p", self);

  LLIST_FOREACH (&self->factories, fnode)
    {
      AcogcFactory f = LLIST_TO_STRUCTP (fnode, acogc_factory, factories);
      /* remove all roots (dynamic objects go to the alive list, uncollectable objects get dropped) */
      LLIST_WHILE_HEAD (&f->roots, i)
        acogc_removeroot (acogc_memory_from_object (LLIST_TO_STRUCTP (i, acogc_object, node)));

      /* remove objects from the freelist */
      LLIST_WHILE_HEAD (&f->dead, i)
        {
          AcogcObject tmp = LLIST_TO_STRUCTP (i, acogc_object, node);
          TRACE_DBG (acogc, "erasing from dead %p", acogc_memory_from_object(tmp));
          if (tmp->factory->finalize)
            tmp->factory->finalize (acogc_memory_from_object (tmp));
          acogc_object_weakref_invalidate (acogc_memory_from_object (tmp));
          llist_unlink (i);
          acogc_free (&tmp);
        }

      /* remove objects in the alive list */
      LLIST_WHILE_HEAD (&f->alive, i)
        {
          AcogcObject tmp = LLIST_TO_STRUCTP (i, acogc_object, node);
          TRACE_DBG (acogc, "erasing from alive %p", acogc_memory_from_object(tmp));
          if (tmp->factory->finalize)
            tmp->factory->finalize (acogc_memory_from_object (tmp));
          acogc_object_weakref_invalidate (acogc_memory_from_object (tmp));
          llist_unlink (i);
          acogc_free (&tmp);
        }

      /* reset factory */
      f->objects_allocated = 0;
      f->objects_used = 0;
      f->objects_new = 0;
    }

  /* reset self */
  self->allocation_counter = 0;
  self->state = ACOGC_STATE_FIRST;
  self->stack = NULL;
}

void
acogc_root_collect (AcogcRoot self, acogc_freeing_policy pol)
{
  REQUIRE (self);
  NOTICE (acogc_collect, "self %p, policy %d", self, pol);

  /* we can short-circruit the collection we just did one and no user-code has been run */
  if (self->allocation_counter != 0 || pol > ACOGC_COLLECT_USER1 || pol == ACOGC_COLLECT_DONTFREE)
    {
      self->allocation_counter = 0;

      /* move all previous alive objects to the tmp lists */
      LLIST_FOREACH (&self->factories, fnode)
        {
          AcogcFactory f = LLIST_TO_STRUCTP (fnode, acogc_factory, factories);
          llist_insert_list_before (&f->alive, &f->tmp);
        }

      ++self->state;
      // check for state overflow and reinit GC states (happens *very* rarely, once every 2^31 collections at worst)
      if (self->state == ACOGC_STATE_LAST)
        {
          NOTICE (acogc_collect, "gc state overflow, recycle");
          self->state = ACOGC_STATE_START;
          LLIST_FOREACH (&self->factories, fnode)
            {
              AcogcFactory f = LLIST_TO_STRUCTP (fnode, acogc_factory, factories);
              LLIST_FOREACH (&f->tmp, i)
                LLIST_TO_STRUCTP (i, acogc_object, node)->state = ACOGC_STATE_FIRST;
              LLIST_FOREACH (&f->dead, i)
                LLIST_TO_STRUCTP (i, acogc_object, node)->state = ACOGC_STATE_FIRST;
            }
        }

      // and now go, marking is copying alive objects back to the alive list
      LLIST_FOREACH (&self->factories, fnode)
        {
          AcogcFactory f = LLIST_TO_STRUCTP (fnode, acogc_factory, factories);
          TRACE (acogc_collect, "marking roots in %s", f->name);
          f->objects_used = 0;
          f->objects_new = 0;
          LLIST_FOREACH (&f->roots, i)
            {
              TRACE_DBG (acogc_collect, "root marker %p",
                       acogc_memory_from_object (LLIST_TO_STRUCTP (i, acogc_object, node)));
              acogc_object_markreally (LLIST_TO_STRUCTP (i, acogc_object, node));
              ++f->objects_used;
            }
        }

      // and for all root->stack references
      TRACE (acogc_collect, "marking stack references");
      for (AcogcStack r = self->stack; r; r = r->prev)
        {
          ASSERT (r != r->prev, "stack corruption, maybe ACOGC_STACK_PTR redefinition in a loop");
          TRACE_DBG (acogc_collect, "stack marker %p", r->ptr);
          acogc_object_mark (r->ptr);
        }

      /* move remaining cruft in the tmplist to the freelist */
      LLIST_FOREACH (&self->factories, fnode)
        {
          AcogcFactory f = LLIST_TO_STRUCTP (fnode, acogc_factory, factories);
          NOTICE (acogc_collect, "collected %d %s", llist_count (&f->tmp), f->name);
          llist_insert_list_before (&f->tmp, &f->dead);
        }
    }

  /* now the freeing pass */

  switch (pol)
    {
    case ACOGC_COLLECT_DONTFREE:
    case ACOGC_COLLECT_NORMAL:
    case ACOGC_COLLECT_FORCEMID:
    case ACOGC_COLLECT_FORCELOW:
    case ACOGC_COLLECT_FORCEALL:
      break;
    case ACOGC_COLLECT_USER1:
    case ACOGC_COLLECT_USER2:
    case ACOGC_COLLECT_USER3:
      if (self->nomemhandler)
        self->nomemhandler (self->nomemsupp, pol);
      return;
    case ACOGC_COLLECT_FAILED:
      abort();
    }

  unsigned long had_objects = 0;
  unsigned long freed_objects = 0;

  LLIST_FOREACH (&self->factories, fnode)
    {
      AcogcFactory f = LLIST_TO_STRUCTP (fnode, acogc_factory, factories);

      had_objects += f->objects_allocated;

      unsigned keep_limit = 0;
      switch (pol)
        {
        case ACOGC_COLLECT_DONTFREE:
          keep_limit = 100;
          break;
        case ACOGC_COLLECT_NORMAL:
          keep_limit = f->high_water;
          break;
        case ACOGC_COLLECT_FORCEMID:
          keep_limit = (f->high_water + f->low_water)/2;
          break;
        case ACOGC_COLLECT_FORCELOW:
          keep_limit = f->low_water;
          break;
        case ACOGC_COLLECT_FORCEALL:
          keep_limit = 0;
          break;
        case ACOGC_COLLECT_USER1:
        case ACOGC_COLLECT_USER2:
        case ACOGC_COLLECT_USER3:
        case ACOGC_COLLECT_FAILED:
          NOTREACHED;
        }

      while (!llist_is_empty (&f->dead) && (f->objects_used * 100 / f->objects_allocated > keep_limit))
        {
          ++freed_objects;

          INFO (acogc_collect, "free rate: %lu", f->objects_used * 100 / f->objects_allocated);
          AcogcObject tmp = LLIST_TO_STRUCTP (llist_get_head (&f->dead), acogc_object, node);

          TRACE_DBG (acogc_collect, "freeing %p", acogc_memory_from_object (tmp));

          if (tmp->factory->finalize)
            tmp->factory->finalize (acogc_memory_from_object (tmp));

          acogc_object_weakref_invalidate (acogc_memory_from_object (tmp));

          llist_unlink (&tmp->node);
          acogc_free (&tmp);
          --f->objects_allocated;
        }
      NOTICE (acogc_collect, "after freeing: allocated %lu, used %lu, ", f->objects_allocated,f->objects_used);
    }

  if (freed_objects > 16)
    {
      self->collection_freq = (self->collection_freq * (had_objects-freed_objects) / had_objects)/2 +1;
      TRACE (acogc_collect, "decreased collection freq: %lu", self->collection_freq);
    }

  NOTICE_DBG (acogc_collect, "collection complete");
}


/*
  factory
*/
void
acogc_factory_init (AcogcFactory self,
                    AcogcRoot root,
                    size_t size,
                    unsigned low_water,
                    unsigned high_water,
                    acogc_mark_func mark,
                    acogc_initize_func initize,
                    acogc_finalize_func finalize,
                    const char* name)
{
  REQUIRE (self);
  REQUIRE (root);

  REQUIRE (size > 0);
  REQUIRE (low_water < 100);
  REQUIRE (high_water < 100);
  REQUIRE (low_water <= high_water);

  NOTICE (acogc, "self %p", self);

  llist_init (&self->factories);
  llist_insert_tail (&root->factories, &self->factories);

  llist_init (&self->roots);
  llist_init (&self->alive);
  llist_init (&self->dead);
  llist_init (&self->tmp);

  self->root = root;

  self->objects_allocated = 0;
  self->objects_used = 0;
  self->objects_new = 0;
  self->high_water = high_water;
  self->low_water = low_water;
  self->size = size;
  self->mark = mark;
  self->initize = initize;
  self->finalize = finalize;
  self->name = name;
}


void *
acogc_factory_alloc (AcogcFactory self)
{
  REQUIRE (self);
  AcogcObject object;

  INFO (acogc_alloc);

  ++self->root->allocation_counter;

  /* maybe call a collection if the freelist is empty */
  if (llist_is_empty (&self->dead) &&
      self->root->allocation_counter > self->root->collection_freq)
    {
      acogc_root_collect (self->root, ACOGC_COLLECT_NORMAL);
      if (self->objects_allocated &&
          100 - (self->objects_used * 100 / self->objects_allocated) < self->low_water)
        {
          // collected less than low_water, increase collection_freq and allocate
          self->root->collection_freq = self->root->collection_freq*3/2 +1;
          TRACE (acogc_collect, "increased collection freq: %lu", self->root->collection_freq);
          goto allocate;
        }
    }

  if (llist_is_empty (&self->dead))
    {
      /* allocate a new object */
    allocate:
      object = NULL;
      acogc_alloc (&object, sizeof (acogc_object) + self->size, self->root);

      llist_init (&object->node);
      llist_insert_tail (&self->alive, &object->node);

      ++self->objects_allocated;
      TRACE_DBG (acogc_alloc, "from malloc %p", acogc_memory_from_object (object));
    }
  else
    {
      /* get one from free-queue */
      llist_insert_tail (&self->alive, llist_get_head (&self->dead));

      object = LLIST_TO_STRUCTP(llist_get_tail (&self->alive), acogc_object, node);

      if (self->finalize)
        self->finalize (acogc_memory_from_object (object));

      acogc_object_weakref_invalidate (acogc_memory_from_object (object));

      TRACE_DBG (acogc_alloc, "from freelist %p", acogc_memory_from_object (object));
    }

  if (self->initize)
    self->initize (acogc_memory_from_object (object));

  object->factory = self;

  object->weakrefs = (AcogcWeakref)&object->weakrefs;
  object->state = self->root->state;

  ++self->objects_new;
  ++self->objects_used;

  return acogc_memory_from_object (object);
}

/*
  object
*/
acogc_mark_result
acogc_object_markreally (AcogcObject self)
{
  REQUIRE (self);

 tailcall:
  TRACE (acogc_mark, "mark object %p, state %d", acogc_memory_from_object (self), self->state);

  REQUIRE (self->state != self->factory->root->state, "marking already marked object %p", acogc_memory_from_object (self));

  if (!self->factory->mark || self->factory->mark (acogc_memory_from_object (self)) == ACOGC_KEEP)
    {
      TRACE_DBG (acogc_mark, "keep object %p", acogc_memory_from_object (self));
      if (self->state >= ACOGC_STATE_BUSY)
        {
          /* is a dynamically allocated object */
          ++self->factory->objects_used;

          /* is a collectable object, store it in the alive list */
          llist_insert_tail (&self->factory->alive, &self->node);
          self->state = self->factory->root->state;
        }

      if (self->factory->root->last)
        {
          self = self->factory->root->last;
          self->factory->root->last = NULL;
          TRACE (acogc_mark, "tailcall");
          goto tailcall;
        }
      return ACOGC_KEEP;
    }
  return ACOGC_COLLECT;
}

/* register a GC root object */
void
acogc_addroot (void* object)
{
  REQUIRE (object);
  TRACE (acogc, "object: %p", object);
  //if(!object)
  //  return;

  AcogcObject o = acogc_object_from_memory (object);

  if (o->state <= ACOGC_STATE_ROOT)
    {
      /* nested addroot */
      --o->state;
      INFO_DBG (acogc, "object: %p already root, nested %d", object, -o->state);
    }
  else
    {
      llist_insert_tail (&o->factory->roots, &o->node);
      if (o->state >= ACOGC_STATE_FIRST)
        o->state = ACOGC_STATE_ROOT;
    }
}

/* unregister a GC root object, collectable objects go back to the alive list, uncollectable objects are reset and removed from the GC */
void
acogc_removeroot (void* object)
{
  REQUIRE (object);

  TRACE (acogc, "object: %p", object);
  //if(!object)
  //  return;

  AcogcObject o = acogc_object_from_memory (object);

  if (o->state < ACOGC_STATE_ROOT)
    {
      /* dynamic object, nested */
      ++o->state;
      INFO_DBG (acogc, "object: %p still root, nested %d", object, -o->state);
    }
  else
    {
      AcogcFactory factory = o->factory;
      if (o->state == ACOGC_STATE_ROOT)
        {
          /* dynamic object, removeroot */
          TRACE_DBG (acogc, " ..dynamic, removed from root");
          llist_insert_tail (&factory->alive, &o->node);
          o->state = factory->root->state;
        }
      else
        {
          /* uncollectable object */
          TRACE_DBG (acogc, " ..uncollectable, removed from root");
          if (o->state == ACOGC_STATE_UNCOLLECTABLE && factory->finalize)
            factory->finalize (object);

          acogc_object_weakref_invalidate (object);

          if (factory->initize)
            factory->initize (object);

          llist_unlink (&o->node);
        }
    }
}


/*
  weakrefs
*/
void
acogc_weakref_link (AcogcWeakref self, void* o)
{
  REQUIRE (self);
  REQUIRE (o);

  TRACE (acogc_weak, "%p to %p", self, o);

  if (self->ref != o)
    {
      NOTICE (acogc_weak, "already linked together");
      if(self->next)
        {
          NOTICE (acogc_weak, "weakref linked to another object");
          acogc_weakref_unlink (self);
        }

      self->ref = o;

      AcogcObject object = acogc_object_from_memory (o);
      ASSERT (object->weakrefs, "weakref not initialized");
      self->next = object->weakrefs;
      object->weakrefs = self;
    }
}

void
acogc_weakref_unlink (AcogcWeakref self)
{
  REQUIRE (self);

  TRACE (acogc_weak, "%p", self);

  AcogcWeakref_ref prev = &self->next;
  AcogcWeakref i = self->next;
  if (i)
    {
      while (i != self)
        {
          ASSERT (i->next, "zero in weak cycle");
          prev = &i->next;
          i = i->next;
        }
      TRACE_DBG (acogc_weak, "unlink %p from after %p, before %p", self, prev, self->next);
      *prev = self->next;
      self->ref = NULL;
      self->next = NULL;
    }
  else
    {
      ASSERT (!self->ref, "weakref not linked");
    }
}

void
acogc_object_weakref_invalidate (void* memory)
{
  REQUIRE (memory);
  TRACE (acogc_weak, "%p, weak %p", memory, acogc_object_from_memory(memory)->weakrefs);

  AcogcWeakref_ref object = &acogc_object_from_memory(memory)->weakrefs;
  if (*object)
    {
      while ((*object)->next != *object)
        {
          AcogcWeakref tmp = *object;
          TRACE_DBG (acogc_weak, "invalidate %p, ref %p", tmp, tmp->ref);
          (*object) = (*object)->next;
          tmp->ref = NULL;
          tmp->next = NULL;
        }
    }
}


void *
acogc_weakref_reinstget (AcogcWeakref self)
{
  REQUIRE (self);
  TRACE (acogc_weak, "%p", self);

  if (!self->ref)
    {
      TRACE_DBG (acogc_weak, "reinst miss %p", self);
      return NULL;
    }

  AcogcObject o = acogc_object_from_memory (self->ref);

  if (o->state >= ACOGC_STATE_FIRST && o->state < o->factory->root->state)
    {
      TRACE_DBG (acogc_weak, "reinst hit %p, object %p", self, self->ref);
      o->state = o->factory->root->state;
      llist_insert_tail (&o->factory->alive, &o->node);
      ++o->factory->objects_used;
    }
  return self->ref;
}

void *
acogc_weakref_get (AcogcWeakref self)
{
  REQUIRE (self);
  TRACE (acogc_weak, "%p", self);

  if (!self->ref)
    return NULL;

  AcogcObject o = acogc_object_from_memory (self->ref);

  if (o->state >= ACOGC_STATE_FIRST && o->state < o->factory->root->state)
    {
      INFO_DBG (acogc_weak, "already invalidated");
      return NULL;
    }

  INFO_DBG (acogc_weak, "resolved %p", self->ref);

  return self->ref;
}

void
acogc_weakref_assert_cleared (AcogcWeakref p)
{
  REQUIRE (p);
  (void) p;
  ASSERT (!p->next && ! p->ref, "weak-weakref %p not cleared at end of scope", p);
}

/* malloc / free wrapers */
void
acogc_alloc_ (void ** addr, size_t size, AcogcRoot root)
{
  REQUIRE (addr);
  REQUIRE (size > 0);
  TRACE (acogc_alloc);

  void * ret;
  acogc_freeing_policy pol = ACOGC_COLLECT_NORMAL;

  /* alloc memory */
 retry:
  ret = realloc (*addr, size);

  if (!ret)
    {
      if (root)
        {
          acogc_root_collect (root, pol);
          ++pol;
          goto retry;
        }
      else
        {
          ERROR (NOBUG_ON, "out of memory");
          abort();
        }
    }

  *addr = ret;
}


size_t
acogc_reserve_ (void ** addr, size_t actual, size_t needed, AcogcRoot root)
{
  REQUIRE (addr);
  REQUIRE (actual > 0);
  REQUIRE (needed > 0);
  TRACE (acogc_alloc);

  void * ret;
  acogc_freeing_policy pol = ACOGC_COLLECT_NORMAL;

  size_t n;
  for (n = actual>16 ? actual : 16; n <= needed; n += (n>>1)); /*n = n * 1.5*/

 retry:
  PLANNED("realloc only 'needed' when memory is low");
  ret = realloc (*addr, n);
  if (!ret)
    {
      if (root)
        {
          n = needed;
          acogc_root_collect (root, pol);
          ++pol;
          goto retry;
        }
      else
        {
          ERROR (NOBUG_ON, "out of memory");
          abort();
        }
    }

  *addr = ret;

  return n;
}


void
acogc_free_ (void ** addr)
{
  REQUIRE (addr);
  TRACE (acogc_alloc);
  free (*addr);
  *addr = NULL;
}

/*
  assertion for cleanup
*/
void
acogc_assert_stackframeleft (AcogcStack_ref p)
{
  REQUIRE (p);
  REQUIRE (*p);
  ASSERT (*p == (AcogcStack) p);
  (void) p;
}

/*
// Local Variables:
// mode: C
// c-file-style: "gnu"
// End:
// arch-tag: 2a29f53b-906e-4025-bf90-8e12b4182474
// end_of_file
*/
