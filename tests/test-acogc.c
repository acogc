/*
  test-acogc.c - simple accurate/cooperative garbage collector

  Copyright (C) 2006, Christian Thaeter <chth@gmx.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 2 as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, contact me.

*/

#include <stdlib.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>

#include "../lib/acogc.h"

struct object
{
  int notused;
  struct object* ptra;
  struct object* ptrb;
  //llist foo;
  int test;
  char pad[20];
};

void
object_init_acogc (void* obj)
{
  struct object* o = obj;
  o->ptra = NULL;
  o->ptrb = NULL;
  //llist_init (&o->foo);
}

acogc_mark_result
object_mark_acogc (void* obj)
{
  struct object* o = obj;
  acogc_object_mark (o->ptra);
  acogc_object_mark (o->ptrb);
  //if (!llist_is_empty (&o->foo))
  //  acogc_object_mark (LLIST_TO_STRUCTP (&o->foo, struct object, foo));
  return ACOGC_KEEP;
}

acogc_root gcroot;
acogc_factory gcfactory;

//ACOGC_OBJECT (UNCOLLECTABLE_NONFINALIZEABLE, &gcfactory, struct object, testobjectglobal, {345, NULL, NULL, LLIST_INITIALIZER(testobjectglobal_acogc.object.foo), 123, "test"});

ACOGC_OBJECT (UNCOLLECTABLE_NONFINALIZEABLE, &gcfactory, struct object, testobjectglobal, {345, NULL, NULL, 123, "test"});


void init()
{
  acogc_root_init(&gcroot);

  acogc_factory_init(&gcfactory,
                     &gcroot,
                     sizeof(struct object),
                     25,
                     50,
                     (acogc_mark_func)object_mark_acogc,
                     (acogc_initize_func)object_init_acogc,
                     NULL,
                     "object");
}

int main(int argc, char* argv[])
{
  struct object* obj;
  struct object* obj1;
  struct object* obj2;

  acogc_nobug_init();

  if (argc != 2)
    {
      printf("ERROR\n");
      exit(1);
    }

  if (strcmp(argv[1], "INIT") == 0)
    {
      printf("init\n");
      init ();
    }
  else if (strcmp(argv[1], "INIT_EMPTY_ERASE") == 0)
    {
      printf("init-empty-erase\n");
      init ();
      acogc_root_erase (&gcroot);
    }
  else if (strcmp(argv[1], "ERASE_SOME_OBJECTS") == 0)
    {
      printf("erase\n");
      init ();
      obj = acogc_factory_alloc (&gcfactory);
      acogc_addroot (obj);
      NOTICE (NOBUG_ON, "allocated %p", obj);

      obj2 = acogc_factory_alloc (&gcfactory);
      obj->ptra = obj2;
      NOTICE (NOBUG_ON, "allocated %p", obj2);

      for (int j = 0; j < 1000; ++j)
        {
          obj1 = acogc_factory_alloc (&gcfactory);
          obj2->ptra = obj1;
          NOTICE (NOBUG_ON, "allocated %p", obj1);

          obj2 = acogc_factory_alloc (&gcfactory);
          obj1->ptra = obj2;
          NOTICE (NOBUG_ON, "allocated %p", obj2);
        }
      acogc_removeroot (obj);

      acogc_root_erase (&gcroot);
    }
  else if (strcmp(argv[1], "RANDOM") == 0)
    {
      printf("random\n");
      init ();
      obj = acogc_factory_alloc (&gcfactory);
      acogc_addroot (obj);
      NOTICE (NOBUG_ON, "allocated %p", obj);

      obj2 = acogc_factory_alloc (&gcfactory);
      acogc_addroot (obj2);
      obj->ptra = obj2;
      NOTICE (NOBUG_ON, "allocated %p", obj2);

      for (int j = 0; j < 1000; ++j)
        {
          obj1 = acogc_factory_alloc (&gcfactory);
          acogc_addroot (obj1);
          if (rand()%2)
            obj2->ptra = obj1;
          if (rand()%2)
            obj2->ptrb = obj1;
          acogc_removeroot (obj2);
          if (rand()%2)
            obj->ptra = obj1;
          NOTICE (NOBUG_ON, "allocated %p", obj1);

          obj2 = acogc_factory_alloc (&gcfactory);
          acogc_addroot (obj2);
          if (rand()%2)
            obj1->ptra = obj2;
          if (rand()%2)
            obj1->ptrb = obj2;
          acogc_removeroot (obj1);
          if (rand()%2)
            obj->ptrb = obj2;
          NOTICE (NOBUG_ON, "allocated %p", obj2);
        }
      acogc_removeroot (obj);

      acogc_root_erase (&gcroot);
    }
  else if (strcmp(argv[1], "WEAK_REFS") == 0)
    {
      printf("weak-refs\n");
      init ();

      obj1 = acogc_factory_alloc (&gcfactory);

      // declare reference weak_a
      acogc_weakref weak_a = ACOGC_WEAKREF_INITIALIZER;

      // declare reference weak_b
      acogc_weakref weak_b = ACOGC_WEAKREF_INITIALIZER;

      // declare reference weak_c
      acogc_weakref weak_c = ACOGC_WEAKREF_INITIALIZER;

      // unlink unused weak_b
      acogc_weakref_unlink (&weak_b);
      assert (weak_b.ref == NULL);
      assert (weak_b.next == NULL);

      // link weak_a
      acogc_weakref_link (&weak_a, obj1);
      assert (weak_a.ref == obj1);

      //link weak_b
      acogc_weakref_link (&weak_b, obj1);
      assert(weak_b.ref == obj1);
      
      //link weak_c
      acogc_weakref_link (&weak_c, obj1);
      assert (weak_c.ref == obj1);
      
      //unlink weak_a
      acogc_weakref_unlink (&weak_a);
      assert (weak_a.ref == NULL);
      assert (weak_a.next == NULL);

      //unlink weak_b
      acogc_weakref_unlink (&weak_b);
      assert (weak_b.ref == NULL);
      assert (weak_b.next == NULL);

      // link weak_b
      acogc_weakref_link (&weak_b, obj1);
      assert (weak_b.ref == obj1);
      assert (weak_b.next == &weak_c);

      // link weak_a
      acogc_weakref_link (&weak_a, obj1);
      assert (weak_a.ref == obj1);
      assert (weak_a.next == &weak_b);

      // link weak_a again
      acogc_weakref_link (&weak_a, obj1);
      assert (weak_a.ref == obj1);
      assert (weak_a.next == &weak_b);

      // test invalidate
      acogc_object_weakref_invalidate (obj1);
      assert (weak_a.ref == NULL);
      assert (weak_a.next == NULL);
      assert (weak_b.ref == NULL);
      assert (weak_b.next == NULL);

      // relink for erase
      acogc_weakref_link (&weak_b, obj1);
      acogc_weakref_link (&weak_a, obj1);

      acogc_root_erase (&gcroot);

      assert (weak_a.ref == NULL);
      assert (weak_a.next == NULL);
      assert (weak_b.ref == NULL);
      assert (weak_b.next == NULL);
    }
  else if (strcmp(argv[1], "REINSTANTIATE") == 0)
    {
      printf("reinst\n");
      init ();

      obj1 = acogc_factory_alloc (&gcfactory);

      acogc_weakref weak = ACOGC_WEAKREF_INITIALIZER;

      acogc_weakref_link (&weak, obj1);

      obj2 = acogc_weakref_reinstget (&weak);
      assert (obj2 == obj1);

      assert (acogc_weakref_get (&weak) != NULL);
      acogc_root_collect (&gcroot, ACOGC_COLLECT_DONTFREE);

      assert (acogc_weakref_get (&weak) == NULL);

      obj2 = acogc_weakref_reinstget (&weak);
      assert (obj2 == obj1);
      assert (acogc_weakref_get (&weak) != NULL);

      acogc_root_erase (&gcroot);
      }
  else if (strcmp(argv[1], "UNCOLLECTABLE") == 0)
    {
      printf("uncollectable\n");
      init ();

      //ACOGC_OBJECT (UNCOLLECTABLE, &gcfactory, struct object, testobject,
      //{345, NULL, NULL, LLIST_INITIALIZER(testobject_acogc.object.foo), 123, "test"});
      ACOGC_OBJECT (UNCOLLECTABLE, &gcfactory, struct object, testobject,
      {345, NULL, NULL, 123, "test"}); 
    
      acogc_addroot (testobject);
      acogc_root_collect (&gcroot, ACOGC_COLLECT_DONTFREE);
      acogc_addroot (testobjectglobal);
      acogc_root_collect (&gcroot, ACOGC_COLLECT_DONTFREE);

      obj = acogc_factory_alloc(&gcfactory);
      testobject->ptra = obj;
      acogc_root_collect (&gcroot, ACOGC_COLLECT_DONTFREE);

      obj = acogc_factory_alloc(&gcfactory);
      testobjectglobal->ptrb = obj;
      acogc_root_collect (&gcroot, ACOGC_COLLECT_DONTFREE);

      testobject->ptra = NULL;
      testobjectglobal->ptrb = NULL;
      acogc_root_collect (&gcroot, ACOGC_COLLECT_DONTFREE);

      obj = acogc_factory_alloc(&gcfactory);
      testobject->ptra = obj;

      obj = acogc_factory_alloc(&gcfactory);
      testobjectglobal->ptrb = obj;

      acogc_root_erase (&gcroot);
      /* again, gcroot should be reuseable now */
      assert(testobject->ptra == NULL);
      assert(testobjectglobal->ptrb == NULL);

      acogc_addroot (testobject);
      acogc_addroot (testobjectglobal);

      obj = acogc_factory_alloc(&gcfactory);
      testobject->ptra = obj;

      obj = acogc_factory_alloc(&gcfactory);
      testobjectglobal->ptrb = obj;

      testobject->ptra = NULL;
      testobjectglobal->ptrb = NULL;

      acogc_root_erase (&gcroot);
    }
  else if (strcmp(argv[1], "NESTED_ROOT") == 0)
    {
      printf("nestedroot\n");
      init ();

      obj = acogc_factory_alloc (&gcfactory);
      acogc_addroot (obj);
      acogc_root_collect (&gcroot, ACOGC_COLLECT_DONTFREE);
      acogc_addroot (obj);
      acogc_root_collect (&gcroot, ACOGC_COLLECT_DONTFREE);
      acogc_addroot (obj);
      acogc_root_collect (&gcroot, ACOGC_COLLECT_DONTFREE);

      acogc_removeroot (obj);
      acogc_root_collect (&gcroot, ACOGC_COLLECT_DONTFREE);
      acogc_removeroot (obj);
      acogc_root_collect (&gcroot, ACOGC_COLLECT_DONTFREE);
      acogc_removeroot (obj);

      acogc_root_erase (&gcroot);
    }
  else
    {
      printf("ERROR\n");
      exit(1);
    }
  return 0;
}
/*
// Local Variables:
// mode: C
// c-file-style: "gnu"
// End:
// arch-tag: ef8371dc-4862-4bee-9b57-080a66205ef9
// end_of_file
*/
