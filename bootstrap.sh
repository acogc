#!/bin/sh
#
#	GNU autotools driver
#
# Copyright (C) 2001, 2004, Christian Thaeter <chth@gmx.net>
#
# hint: install this somewhere in your PATH as 'autobootstrap'
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, contact me.
#

function chk_prg()
{
    test ! "$chk_prg_org" && chk_prg_org="$*"
    test $# -eq 0 && { echo "lacking on of $chk_prg_org"; exit 1;}
    if test ! "$1"; then
        echo "";
    else
        local prg
        prg=$(which "$1")
        if test "$prg" = ""; then
            shift;
            chk_prg "$@"
        else
            chk_prg_org="";
            echo "$prg"
        fi
    fi
}

#default options (configure this for your package)
DEF_BUILDDIR='=build'
DEF_BUILDOPT='-j 9'
DEF_CFLAGS_DEBUG='-O0 -g -W -Wall -Werror -std=gnu99'
DEF_CFLAGS_RELEASE='-O3 -W -Wall -Werror -std=gnu99'
DEF_CONFIGUREFLAGS_DEBUG=''
DEF_CONFIGUREFLAGS_RELEASE='--enable-nobug=RELEASE'
DEF_CC="nice $(chk_prg ccache "") $(chk_prg distcc "") $(chk_prg gcc-3.4 gcc cc)"

#default programs (probably no need for configuration)
AUTOCONFVERSION='2.50'
AUTOMAKEVERSION='-1.9'

MAKE="$(chk_prg gmake make)"
AUTOSCAN="$(chk_prg autoscan$AUTOCONFVERSION autoscan)"
ACLOCAL="$(chk_prg aclocal$AUTOMAKEVERSION aclocal)"
AUTOCONF="$(chk_prg autoconf$AUTOCONFVERSION autoconf)"
AUTOHEADER="$(chk_prg autoheader$AUTOCONFVERSION autoheader)"
AUTOMAKE="$(chk_prg automake$AUTOMAKEVERSION)"


#commandline parser (needs to be done better / options etc)
do_mrproper=false
do_release=false
do_nobootstrap=false
do_stop=false
do_configure=false
do_chelp=false
do_build=false
do_check=false
do_install=false
do_clean=false
do_uninstall=false
do_dist=false
do_dir=true
dir_opt="$DEF_BUILDDIR"
base_dir=`pwd`

for i in "$@"; do
    case $i in
	mrproper|distclean)
	    do_mrproper=true
	    do_uninstall=true
	    do_stop=true
	    ;;
	rebuild)
	    do_mrproper=true
	    do_uninstall=true
	    do_stop=false
	    ;;
	nobootstrap)
	    do_nobootstrap=true
	    ;;
	chelp)
	    do_chelp=true
	    do_stop=false
	    ;;
	configure*)
	    configure_opt=${i#configure}
	    do_configure=true
	    do_stop=false
	    ;;
	make*)
	    build_opt=${i#make}
	    do_build=true
	    do_configure=true
	    do_stop=false
	    ;;
	build*)
	    build_opt=${i#build}
	    do_build=true
	    do_configure=true
	    do_stop=false
	    ;;
	nocd|intree)
	    do_dir=false
            ;;
	cd*)
	    dir_opt=${i#cd}
	    dir_opt="${dir_opt:-$DEF_BUILDDIR}"
	    do_dir=true
	    ;;
	check|test)
	    do_check=true
	    do_configure=true
	    do_stop=false
	    ;;
	install)
            do_release=true
	    do_install=true
	    do_check=true
	    do_configure=true
	    do_uninstall=true
	    do_stop=false
	    ;;
	clean)
	    do_clean=true
	    do_stop=false
	    ;;
	uninstall)
	    do_uninstall=true
	    do_stop=false
	    ;;
	dist)
            do_release=true
	    do_dist=true
	    do_configure=true
	    do_clean=true
	    do_stop=false
	    ;;
	release)
	    do_release=true
	    ;;
	help*|-h*)
	    echo "bootstrap a GNU-Autotools Project"
	    echo "usage: bootstrap options..."
	    echo "        options can be one or more of:"
	    echo "        mrproper    - big-clean the project, try's to uninstall previous version"
	    echo "        rebuild     - mrproper and rebuild from scratch"
	    echo "        nobootstrap - don't use autotools, use existing configure"
	    echo "        intree      - do a build within the sourcetree *BAD!*"
	    echo "        cd*         - build into a subdir default: ./=build{-release|-debug}"
	    echo "        chelp       - shows ./configure --help, interactively asks for configure options"
	    echo "        configure*  - bootstraps the project including ./configure"
	    echo "        make*       - does a 'make' as well"
	    echo "        check       - additionally runs tests"
	    echo "        install     - installs project if checks are passed"
	    echo "        clean       - cleans up after building"
	    echo "        uninstall   - try to uninstall allready installed version before proceeding"
	    echo "        dist        - builds a distribution if all is sane"
	    echo "        release     - uses 'release' configuration"
            echo "                      default: debug for check, release for install"
	    echo "        help        - this sceen"
	    echo "    cd, configure and make can be quoted and appended with options"
	    echo "    example:"
	    echo "        bootstrap rebuild 'cd build' 'configure --with-x' 'make -j4' dist"
	    exit 0
	    ;;
    esac
done

#set some vars to defaults
if test ! "$build_opt"; then
    build_opt="$DEF_BUILDOPT"
fi

if test ! "$CONFIGUREFLAGS"; then
    if test $do_release = true; then
        CONFIGUREFLAGS="$DEF_CONFIGUREFLAGS_RELEASE $EXTRA_CONFIGUREFLAGS"
    else
        CONFIGUREFLAGS="$DEF_CONFIGUREFLAGS_DEBUG $EXTRA_CONFIGUREFLAGS"
    fi
    export CONFIGUREFLAGS
fi

if test ! "$CFLAGS"; then
    if test $do_release = true; then
        CFLAGS="$DEF_CFLAGS_RELEASE $EXTRA_CFLAGS"
        dir_opt="${dir_opt}.release"
    else
        CFLAGS="$DEF_CFLAGS_DEBUG $EXTRA_CFLAGS"
        dir_opt="${dir_opt}.debug"
    fi
    export CFLAGS
fi
if test ! "$CC"; then
    CC="$DEF_CC"
    export CC
fi

# lets go

#first of all we install ourself
if test ! -f "$base_dir/bootstrap.sh"; then
    echo "installing ./bootstrap.sh .."
    cp -a "$0" "$base_dir/bootstrap.sh"
fi

if test $do_nobootstrap = false; then

# old compatibilitry check?
    configure_ac_name='configure.ac'
    if test -f 'configure.in'; then
	echo "using obsolete configure.in.."
	configure_ac_name='configure.in'
    fi

#we need to go into the build dir for deinstalling and mrproper
    if test $do_dir = true; then
	test -d $dir_opt && cd $dir_opt
    fi

# uninstall old version?
    if test $do_uninstall = true; then
	if test -f 'Makefile'; then
	    echo "uninstalling.."
	    $MAKE uninstall
	fi
    fi

# check if we want a really clean mrproper
    if test $do_mrproper = true; then
	echo "mrproper.."
	if test -f 'Makefile'; then
	    $MAKE maintainer-clean
	fi
	cd $base_dir
	rm -f config.* configure Makefile Makefile.in
	test -d "$dir_opt" && chmod -R u+w "$dir_opt" && rm -rf $dir_opt
	test -f $configure_ac_name && touch $configure_ac_name
    fi
    cd $base_dir

# enough?
    if test $do_stop = true; then
	echo "..stopped"
	exit 0
    fi

# do we need to make configure.ac
    if test ! -f $configure_ac_name; then
	echo "generating configure.ac.."
	$AUTOSCAN
	echo "dnl Note from bootstrap:" >$configure_ac_name
	echo "dnl this scan is not yet configured for automake" >>$configure_ac_name
	echo "dnl add at least the following macros" >>$configure_ac_name
	echo "dnl AM_INIT_AUTOMAKE( name , version )" >>$configure_ac_name
	echo "dnl AM_CONFIG_HEADER([config.h])" >>$configure_ac_name
	echo "dnl and don't forget to insert Makefile in AC_CONFIG_FILES()" >>$configure_ac_name
	echo "dnl end_of_note" >>$configure_ac_name
	cat configure.scan >>$configure_ac_name
	rm configure.scan
	echo "Please edit $configure_ac_name hit Ctrl-D when finished"
	sh
	if test ! -f $configure_ac_name; then
	    echo "can't continue"
	    exit 1
	fi
    fi

# do we need to make Makefile.am
    if test ! -f 'Makefile.am'; then
	echo "## Makefile.am" >Makefile.am
	echo "" >>Makefile.am
	echo "##end" >>Makefile.am
	echo "Please edit 'Makefile.am' hit Ctrl-D when finished"
	sh
	if test ! -f 'Makefile.am'; then
	    echo "can't continue"
	    exit 1
	fi
    fi

# generating aclocal.m4
    if test $configure_ac_name -nt 'aclocal.m4'; then
	echo "aclocal.."
	$ACLOCAL
    fi

#run autoconf?
    if test $configure_ac_name -nt 'configure'; then
	echo "autoconf.."
	$AUTOCONF
    fi

#run autoheader?
    if grep '^AM_CONFIG_HEADER' $configure_ac_name >/dev/null && test $configure_ac_name -nt 'config.h.in'; then
	echo "autoheader.."
	$AUTOHEADER
	touch config.h.in
    fi


#run automake?
    if test 'Makefile.am' -nt 'Makefile.in'; then
	echo "automake.."
	test -f NEWS || echo "write me" >NEWS
	test -f README || echo "write me" >README
	test -f AUTHORS || echo "write me" >AUTHORS
	test -f ChangeLog || echo "write me" >ChangeLog
	$AUTOMAKE -a -c --gnu
    fi

#nobootstrap end
fi

#cd?
if test $do_dir = true; then
    test -d $dir_opt || mkdir -p "$dir_opt"
    cd $dir_opt
fi

#configure help?
if test $do_chelp = true; then
    echo "configure help.."
    $base_dir/configure --help
    echo "Please enter configure options and hit [return]:"
    read configure_new
    configure_opt="$configure_opt $configure_new"
    touch $base_dir/configure
fi

#configure?
if test $do_configure = true; then
    if test "$base_dir/configure" -nt 'Makefile'; then
	echo "configure.."
	$base_dir/configure $CONFIGUREFLAGS $configure_opt
    fi
fi

#build?
if test $do_build = true; then
    echo "building.."
    $MAKE $build_opt
fi

#check?
if test $do_check = true; then
    echo "testing.."
    $MAKE $build_opt check || {
        err=$?
        echo ".. test failed!"
        exit $err
    }
fi

#install?
if test $do_install = true; then
    echo "installing.."
    $MAKE install-strip
fi

#clean?
if test $do_clean = true; then
    echo "cleaning.."
    $MAKE clean
fi

#dist?
if test $do_dist = true; then
    echo "make distribution.."
    $MAKE distcheck && $MAKE distclean && $MAKE dist
fi

#cd back
cd $base_dir

echo ".. finished! $(date +%H:%M:%S)"

# arch-tag: 0b599c55-a968-4c54-bb41-c0ba6472f0df
#end
